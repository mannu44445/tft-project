package com.tft.service;

import com.tft.model.User;

public interface RegisterService {

	User registerUser(User user) ;
}
