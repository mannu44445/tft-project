package com.tft.enumeration;

public enum DATE_FORMAT {

		dd_MM_yyyy {
	        @Override
	        public String toString() {
	            return "dd.MM.yyyy";
	        }
	    },
	    yyyy_MM_dd {
	        @Override
	        public String toString() {
	            return "yyyy.MM.dd";
	        }
	    },

	    yyyy_MM_dd_hh_mm_ss {
	        @Override
	        public String toString() {
	            return "yyyy-MM-dd hh:mm:ss";
	        }
	    }
	    
	
}
