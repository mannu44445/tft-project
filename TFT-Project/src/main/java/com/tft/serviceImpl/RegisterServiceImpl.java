package com.tft.serviceImpl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tft.model.User;
import com.tft.repository.UserReposistory;
import com.tft.service.RegisterService;

@Service
public class RegisterServiceImpl implements RegisterService {
	
	private static final Logger log = LoggerFactory.getLogger(RegisterServiceImpl.class);

	UserReposistory userReposistory;
	public RegisterServiceImpl(UserReposistory userReposistory)	{
		super();
		this.userReposistory = userReposistory;
	}
	
	@Override
	public User registerUser(User user) {
		log.info("registerUser servoce "+user);
		User u = userReposistory.save(user);
		log.info("u "+user);
		return u;
	}
	
}
