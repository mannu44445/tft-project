package com.tft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tft.model.User;

@Repository
public interface UserReposistory extends JpaRepository<User, Long>{

}
