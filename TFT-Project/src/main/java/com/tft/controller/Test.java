package com.tft.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
//import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
//import com.ewallet.constant.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tft.model.User;
import com.tft.service.RegisterService;


@RestController
public class Test {
	
	public static final Logger log = LogManager.getLogger(Test.class);
	
	RegisterService registerService;
	
	public Test(RegisterService registerService ) {
		super();
		this.registerService = registerService;
	}
	
	@PostMapping("registeruser")
	public ResponseEntity<User> register(@RequestBody User user) throws Exception{
		User u = registerService.registerUser(user);
		return new ResponseEntity<User>(u, HttpStatus.OK);
	}
	
}
